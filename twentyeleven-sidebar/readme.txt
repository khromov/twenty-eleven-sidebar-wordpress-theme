twentyeleven-sidebar is a twentyeleven child theme with a persistent, always-on sidebar.

Inspired by Chris Aprea at Futurewebblog:
http://futurewebblog.com/add-sidebar-support-posts-twenty-eleven-theme/